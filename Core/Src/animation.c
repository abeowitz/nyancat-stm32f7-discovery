/*
 * animation.c
 *
 *  Created on: Jul 17, 2018
 *      Author: abe
 */

#include <stdio.h>
#include <stdint.h>
#include <animation.h>
#include <main.h>
#include <fatfs.h>
#include "ffconf.h"
#include "ff.h"
#include "stm32746g_discovery_lcd.h"

#define LCD_FB1_LR1_ADDR    ((uint32_t)0xC0000000)
#define LCD_FB1_LR2_ADDR    ((uint32_t)0xC0080000)
#define NYANCAT_BG_COLOR    ((uint32_t)0xFF000030)
#define IMG_RAINBOW_ADDR    ((uint32_t)0xC0100000)
#define IMG_TWSTARS_ADDR    ((uint32_t)0xC0180000)
#define IMG_NYANCAT_ADDR    ((uint32_t)0xC0200000)

extern DMA2D_HandleTypeDef hdma2d;

typedef struct
{
    uint8_t inserted;      // 1 is Inserted
    int mounted;       // 1 is Mounted
    char label[16];
} SDCARDSTATUS;

char tmpString[128];

// Private functions
void drawImage(uint32_t srcaddr, uint32_t dstaddr, uint32_t imgWidth,
        uint32_t imgHeight);
void blendImage(uint32_t srcaddr1, uint32_t srcaddr2, uint32_t dstaddr,
        uint32_t imgWidth, uint32_t imgHeight);

uint32_t file2buffer(uint8_t *dstaddr, char *filename);

// SD Card
DWORD fre_clust, fre_sect, tot_sect;
FRESULT fres;
FILINFO fno;
FATFS *fs;
DIR cdir;
FIL fil;
UINT br, btr;
uint32_t bread;

void animation(void)
{
    // SD Card
    DWORD fre_clust, fre_sect, tot_sect;
    FRESULT fres;
    FILINFO fno;
    FATFS *fs;
    DIR cdir;
    FIL fil;
    uint32_t bread;

    // my SD vars
    uint8_t sd_state;
    uint8_t *bufptr;
    SDCARDSTATUS SDStatus;
    float frames = 0;
    float fps;

    uint32_t dstaddr, srcaddr1;
    uint32_t cf = 0; //catframe
    uint32_t rf = 0; //rainbowframe
    uint32_t sf[5];  // starframes
    uint32_t sfx[5];
    uint32_t sfy[5];
    uint32_t nyancat_bg = NYANCAT_BG_COLOR;

    // Mount SD Card:
    printf("SD %s\r\n", SDPath);
    sd_state = BSP_SD_Init();
    printf("BSP %02X\r\n", sd_state);

    fres = f_mount(&SDFatFS, SDPath, 1);
    printf("Mount: %d\r\n", fres);
    fs = &SDFatFS;

#ifdef _USE_LABEL // in ffconf.h
    fres = f_getlabel(SDPath, SDStatus.label, 0);
#else
    strcpy(SDStatus.label, "None");
#endif
    printf("SD Label: %s\r\n", SDStatus.label);

    // ======================================================
    /* Get volume information and free clusters of drive 1 */
    fres = f_getfree(SDPath, &fre_clust, &fs);

    /* Get total sectors and free sectors */
    tot_sect = (SDFatFS.n_fatent - 2) * SDFatFS.csize;
    fre_sect = fre_clust * SDFatFS.csize;

    /* Print the free space (assuming 512 bytes/sector) */
    printf("%10lu KiB total drive space.\r\n%10lu KiB available.\r\n\r\n",
            tot_sect / 2, fre_sect / 2);

    // ======================================================
    // Test Directory Find
    fres = f_findfirst(&cdir, &fno, "/NyanCat", "*");
    while (fres == FR_OK && fno.fname[0])
    {
        printf(" /NyanCat/%s\r\n", fno.fname);
        fres = f_findnext(&cdir, &fno);
    }
    f_closedir(&cdir);
    printf("\r\n");

    /* LCD Initialization */
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(0, LCD_FB1_LR1_ADDR);
    BSP_LCD_LayerDefaultInit(1, LCD_FB1_LR2_ADDR);

    /* Enable the LCD */
    BSP_LCD_DisplayOn();
    /* Select the LCD Background Layer  */
    BSP_LCD_SelectLayer(0);
    /* Clear the Background Layer */
    BSP_LCD_Clear((uint32_t) 0xFF000030);

    /* Select the next Layer  */
    BSP_LCD_SelectLayer(1);
    /* Clear the Foreground Layer */
    BSP_LCD_Clear((uint32_t) 0x00000000);

    // Configure the DMA2D Mode, Color Mode and output offset
    hdma2d.Init.Mode = DMA2D_M2M;
    hdma2d.Init.ColorMode = DMA2D_OUTPUT_ARGB8888;
    hdma2d.Init.OutputOffset = 0;
    HAL_DMA2D_Init(&hdma2d);

    // Background Configuration
    hdma2d.LayerCfg[0].AlphaMode = DMA2D_NO_MODIF_ALPHA;
    hdma2d.LayerCfg[0].InputAlpha = 0x00;
    hdma2d.LayerCfg[0].InputColorMode = DMA2D_ARGB8888;
    hdma2d.LayerCfg[0].InputOffset = 0;
    HAL_DMA2D_ConfigLayer(&hdma2d, 0);

    hdma2d.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
    hdma2d.LayerCfg[1].InputAlpha = 0x00;
    hdma2d.LayerCfg[1].InputColorMode = DMA2D_ARGB8888;
    hdma2d.LayerCfg[1].InputOffset = 0;
    HAL_DMA2D_ConfigLayer(&hdma2d, 1);
    hdma2d.Instance = DMA2D;

    BSP_LCD_SelectLayer(0);
    BSP_LCD_SetTextColor(NYANCAT_BG_COLOR);


    // ==============================================================
    // Wipe RAM, so we can debug file loading issues
    // i.e. if ram already has data, screen will appear to work properly
    // RAM is 16bits wide on STM32F7-DISCO, so, not 32bit write here.
    // Not sure if 32bits is faster, don't understand word2mem DMA yet.
    bufptr = (uint8_t *)0xC0100000;
    while(bufptr < 0xC0440000)
    {
        (*(uint16_t *)bufptr) = 0x0000;
        bufptr += 2;
    }


    // ==============================================================
    // LOAD ANIM FRAMES INTO SDRAM

    // RAINBOW
    bufptr = (uint8_t *) IMG_RAINBOW_ADDR;
    for (int i = 4; i > 0; i--)
    {
        sprintf(tmpString, "/NyanCat/rainbow%02d.raw", i);
        bread = file2buffer(bufptr, tmpString);
        printf("0x%08lX 0x%08lX %s\r\n", (uint32_t) bufptr, bread, tmpString);
        bufptr += 112896;
    }

    // STARS
    bufptr = (uint8_t *) IMG_TWSTARS_ADDR;
    for (int i = 7; i > 0; i--)
    {
        sprintf(tmpString, "/NyanCat/star%02d.raw", i);
        bread = file2buffer(bufptr, tmpString);
        printf("0x%08lX 0x%08lX %s\r\n", (uint32_t) bufptr, bread, tmpString);        printf("%08lX %lu %s\r\n", (uint32_t) bufptr, bread, tmpString);
        bufptr += 7296;
    }

    // Load cat images into RAM
    bufptr = (uint8_t *) IMG_NYANCAT_ADDR;
    for (int i = 12; i > 0; i--)
    {
        sprintf(tmpString, "/NyanCat/nyancat%02d.raw", i);
        bread = file2buffer(bufptr, tmpString);
        printf("0x%08lX 0x%08lX %s\r\n", (uint32_t) bufptr, bread, tmpString);
        bufptr += 182784;
    }

    // initial star positions
    sf[0] = 0;
    sfx[0] = 460;
    sfy[0] = 100;
    sf[1] = 2;
    sfx[1] = 38;
    sfy[1] = 38;
    sf[2] = 4;
    sfx[2] = 240;
    sfy[2] = 250;
    sf[3] = 5;
    sfx[3] = 300;
    sfy[3] = 150;
    sf[4] = 3;
    sfx[4] = 150;
    sfy[4] = 200;

    seconds = 0.0;
    frames = 0.0;

    // MAIN ANIMATION LOOP
    while (1)
    {

        // calculate frames/sec:
        if (seconds >= 10.0 && frames > 0.0)
        {
            float fps = (float) frames / (float) seconds;
            printf("%6.2f frames in %6.2f seconds = %6.2f fps\r\n", frames,
                    seconds, fps);
            frames = 0.0;
            seconds = 0.0;
        }

        // ================================================================
        // Animate!

        // Wait for vblank trigger, then draw
        while (HAL_GPIO_ReadPin(GPIOI, GPIO_PIN_9) == 0)
            ;
        while (HAL_GPIO_ReadPin(GPIOI, GPIO_PIN_9) == 1)
            ;

        // Clear behind rainbow
        // clearing the whole screen is slow.  Just erase this section.
        //BSP_LCD_SelectLayer(0);
        BSP_LCD_FillRect(0,60,168,168);

        // Draw several stars
        for (int star = 0; star < 5; star++)
        {
            // Stars
            // print blank star if at edge
            if (sfx[star] <= 8 || sfx[star] > 480)
            {
                dstaddr = LCD_FB1_LR1_ADDR + (sfy[star] * 4 * 480)
                        + ((sfx[star] + 8) * 4);
                srcaddr1 = IMG_TWSTARS_ADDR;
                sfx[star] = 470;
            }
            else
            {
                dstaddr = LCD_FB1_LR1_ADDR + (sfy[star] * 4 * 480)
                        + (sfx[star] * 4);
                srcaddr1 = IMG_TWSTARS_ADDR + (sf[star] * 48 * 38 * 4);
            }
            drawImage(srcaddr1, dstaddr, 48, 38);

            // move star to left for next draw
            sfx[star] -= 6;

            // new anim frame
            sf[star]++;
            if (sf[star] > 6)
            {
                sf[star] = 0;
            }
        }

        // RAINBOW
        srcaddr1 = IMG_RAINBOW_ADDR + (rf % 4) * 168 * 168 * 4;
        dstaddr = LCD_FB1_LR1_ADDR + (60 * 4 * 480) + 0;
        blendImage(srcaddr1, dstaddr, dstaddr, 168, 168);
        //drawImage(srcaddr1, dstaddr, 168, 168);
        rf++;
        if (rf > 7)
        {
            rf = 0;
        }

        // CAT
        srcaddr1 = IMG_NYANCAT_ADDR + (cf * 272 * 168 * 4);
        //      srcaddr2 = LCD_FB1_LR1_ADDR + (52 * 4 * 480) + (104 * 4);
        dstaddr = LCD_FB1_LR2_ADDR + (52 * 4 * 480) + (104 * 4);
        drawImage(srcaddr1, dstaddr, 272, 168);
        //      blendImage(srcaddr1, srcaddr2, dstaddr, 272, 168);
        cf++;
        if (cf > 11)
        {
            cf = 0;
        }

        frames++;
        HAL_Delay(60);

    }

    // CATCH ALL
    while (1)
    {
        printf("%08lX\r\n", epochTime);
        HAL_Delay(1000);
    }
}

// DMA2D draw routines

void drawImage(uint32_t srcaddr, uint32_t dstaddr, uint32_t imgWidth,
        uint32_t imgHeight)
{

    hdma2d.Init.Mode = DMA2D_M2M;
    hdma2d.Init.OutputOffset = 480 - imgWidth;
    HAL_DMA2D_Init(&hdma2d);

    hdma2d.LayerCfg[0].InputOffset = 0;
    HAL_DMA2D_ConfigLayer(&hdma2d, 0);

    hdma2d.LayerCfg[1].InputOffset = 0;
    HAL_DMA2D_ConfigLayer(&hdma2d, 1);

    HAL_DMA2D_Start(&hdma2d, srcaddr, dstaddr, imgWidth, imgHeight);
    HAL_DMA2D_PollForTransfer(&hdma2d, 10);

}

void blendImage(uint32_t srcaddr1, uint32_t srcaddr2, uint32_t dstaddr,
        uint32_t imgWidth, uint32_t imgHeight)
{

    hdma2d.Init.Mode = DMA2D_M2M_BLEND;
    hdma2d.Init.OutputOffset = 480 - imgWidth;
    HAL_DMA2D_Init(&hdma2d);

    hdma2d.LayerCfg[0].InputOffset = 480 - imgWidth;
    HAL_DMA2D_ConfigLayer(&hdma2d, 0);

    hdma2d.LayerCfg[1].InputOffset = 0;
    HAL_DMA2D_ConfigLayer(&hdma2d, 1);

    HAL_DMA2D_BlendingStart(&hdma2d, srcaddr1, dstaddr, dstaddr, imgWidth,
            imgHeight);
    HAL_DMA2D_PollForTransfer(&hdma2d, 10);

}

uint32_t file2buffer(uint8_t *dstaddr, char *filename)
{
    FSIZE_t fsize;
    uint32_t total;

    // Open File
    fres = f_open(&fil, filename, FA_READ);
    if (fres > 0)
    {
        printf("ERROR! %u %s\r\n", fres, filename);
        return fres;
    }

    // Get file size
    fsize = f_size(&fil);

    // Read file data into memory address
    br = 1;
    total = 0;
    btr = 4096; //(uint32_t) fsize;
    while (total < fsize && br > 0)
    {
        fres = f_read(&fil, dstaddr + total, btr, &br);
        //HAL_Delay(250);
        total += br;
    }
    //printf("  %u  %u  %u \r\n", total, btr, br);

    f_close(&fil);

    if (total < fsize || fres > 0)
    {
        printf("FS Error:  %u  %lu\r\n", fres, total);
    }

    return total;
}
